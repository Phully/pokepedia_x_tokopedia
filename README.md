# Pokepedia

> This application create based on mobile first design strategy and best view with chrome simulate device mode mobile


# Demo 
https://pokepedia-x-tokopedia.vercel.app/

# Information 

1. I Keep my pokemon data in local storage (my-pokemon)
2. By deleting local storage  (catch-history), it is possible to catch multiple pokemon
3. You can refresh nearby pokemon by deleting cookies (nearby)
4. I'm utilizing pokeapi's graphql (https://beta.pokeapi.co/graphql/v1beta) because it's still in beta, there's a chance the data will differ from the rest api.
5. All GraphQL Call fetch policy is cache-first.
6. Lazy Load pokemon detail radar and recommendation based on user viewport to increase performance.


# Screenshoot (/screenshot for further information)

![Welcome](screenshot/6.png "Welcome")

![Welcome](screenshot/1.png "Welcome")

![Welcome](screenshot/2.png "Welcome")

![Welcome](screenshot/3.png "Welcome")

![Welcome](screenshot/4.png "Welcome")

![Welcome](screenshot/5.png "Welcome")

