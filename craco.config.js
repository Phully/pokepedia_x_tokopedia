const { CracoAliasPlugin } = require('react-app-alias');

module.exports = {
  plugins: [{ plugin: CracoAliasPlugin, options: {} }],
  babel: { presets: ['@emotion/babel-preset-css-prop'] },
};
