import React from 'react';

import { Global } from '@emotion/react';
import { CookiesProvider } from 'react-cookie';
import { ErrorBoundary } from 'react-error-boundary';
import { ApolloProvider } from '@apollo/client';

import { Space, SpinLoading } from 'antd-mobile';

import RoutesApp from '@/RoutesApp';
import ErrorPage from '@/components/ErrorPage';

import client from '@/utils/config/apollo-client';
import styles from '@/styles';

const App = () => {
  return (
    <React.StrictMode>
      <Global styles={styles} />
      <React.Suspense
        fallback={
          <Space className={'main-loader'} justify={'center'} align={'center'} block={true}>
            <Global styles={styles} />
            <SpinLoading color="primary" />
          </Space>
        }
      >
        <ErrorBoundary fallbackRender={() => <ErrorPage type={'SOMETHING_ERROR'} />}>
          <CookiesProvider>
            <ApolloProvider client={client}>
              <RoutesApp />
            </ApolloProvider>
          </CookiesProvider>
        </ErrorBoundary>
      </React.Suspense>
    </React.StrictMode>
  );
};

export default App;
