import React from 'react';

import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

import { Main, Profile, MyPokemon } from '@/pages/Home';
import { ListPokemon, DetailPokemon } from '@/pages/Pokemon';

import LayoutHome from '@/layouts/Home';
import LayoutSearch from '@/layouts/Search';
import LayoutNavigation from '@/layouts/Navigation';

import ErrorPage from '@/components/ErrorPage';

const RoutesApp = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route index={true} element={<Navigate replace to="/home" />} />
          <Route path={'home'} element={<LayoutHome />}>
            <Route index={true} element={<Main />} />
            <Route path={'widget'} element={<Main />} />
            <Route path={'my-pokemon'} element={<MyPokemon />} />
            <Route path={'profile'} element={<Profile />} />
          </Route>

          <Route path={'pokemon-list'} element={<LayoutSearch />}>
            <Route index={true} element={<ListPokemon />} />
          </Route>

          <Route path={'pokemon/:pokemonName'} element={<LayoutNavigation />}>
            <Route index={true} element={<DetailPokemon />} />
          </Route>
        </Route>
        <Route path={'*'} element={<LayoutHome />}>
          <Route path="*" element={<ErrorPage type={'NOT_FOUND_PAGE'} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default RoutesApp;
