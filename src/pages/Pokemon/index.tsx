import React from 'react';

export const DetailPokemon = React.lazy(() => import('@/pages/Pokemon/containers/Detail'));

export const ListPokemon = React.lazy(() => import('@/pages/Pokemon/containers/List'));
