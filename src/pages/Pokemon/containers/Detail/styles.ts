import { css } from '@emotion/react';

export const container = css`
  display: block;
  width: 100%;
  padding-bottom: 8px;
`;

export const space = css`
  --gap-vertical: 18px;
`;

export const nameContainer = css`
  display: flex;
  align-items: baseline;
  margin: 0 0 14px;
`;

export const title = css`
  font-size: 22px;
  margin: 0;
`;

export const aliasName = css`
  font-size: 18px;
  margin: 0 0 0 8px;
  font-weight: 300;
`;

export const description = css`
  font-size: 14px;
  line-height: 22px;
  width: 85%;
  font-weight: 200;
  margin: 0;
`;

export const infoMargin = css`
  margin: 4px 0;
`;

export const radarContainer = css`
  height: 300px;
  margin-top: -25px;
  padding-right: 50px;
  padding-left: 50px;
`;

export const avatar = css`
  margin: 0 auto 20px;
  .adm-image {
    margin: auto;
  }
  .adm-image-tip {
    background-color: var(--primary-background-color);
    border-radius: 30px;
    box-shadow: inset 0 0 0 1px var(--adm-color-primary);
  }
`;

export const moveCard = css`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  width: 100%;
  height: 40px;
  background-color: var(--secondary-background-color);
  border-radius: 12px;
  border: 1px solid var(--adm-color-primary);
`;

export const wrapperFloating = css`
  display: block;
  width: 100%;
`;

export const innerFloating = css`
  margin: 20px;
`;

export const floating = css`
  background-color: var(--primary-background-color) !important;
  .adm-floating-panel::after {
    background-color: var(--primary-background-color) !important;
  }
`;

export const loader = css`
  margin: auto;
`;

export const loaderRadar = css`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  margin: auto;
`;

export const recommendation = css`
  margin-top: -35px;
`;
