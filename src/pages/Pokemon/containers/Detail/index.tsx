import React, { memo, useRef } from 'react';
import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';

import { Image, Space, SpinLoading } from 'antd-mobile';

import ErrorPage from '@/components/ErrorPage';
import HeaderTitle from '@/components/HeaderTitle';
import PokeMove from '@/pages/Pokemon/components/PokeMove';
import EmblaInfo from '@/pages/Pokemon/components/EmblaInfo';
import PokeCardInfo from '@/components/PokeCard/PokeCardInfo';
import PokemonMove from '@/pages/Pokemon/containers/Detail/PokemonMove';

import { useCreation, useInViewport } from 'ahooks';
import { useLocation, useParams } from 'react-router-dom';
import { usePokemonDetailQuery } from '@/codegen/pages';

import radarConfig from '@/utils/config/radar';
import pokeDescription from '@/utils/functions/poke-description';
import transformPokeAvatar from '@/utils/functions/poke-avatar';

import type { PokemonMoveRef } from '@/pages/Pokemon/containers/Detail/PokemonMove';

import * as styles from '@/pages/Pokemon/containers/Detail/styles';

const Radar = React.lazy(
  () => /* webpackPrefetch: true */ import('@ant-design/plots/es/components/radar'),
);

const PokeRecommendation = React.lazy(
  () => /* webpackPrefetch: true */ import('@/pages/Home/containers/Widget/PokeRecommendation'),
);

const Detail = () => {
  const params = useParams();
  const location = useLocation();
  const radarRef = useRef(null);
  const pokemonMoveRef = useRef<PokemonMoveRef>(null);
  const [radarInfViewPort] = useInViewport(radarRef);
  const pokemonQuery = usePokemonDetailQuery({
    variables: { name: params?.pokemonName?.toLowerCase() || '' },
  });

  const pokemon = useCreation(() => {
    return pokemonQuery.data?.pokemonDetail?.[0];
  }, [pokemonQuery.data]);

  if (pokemonQuery.loading) return <SpinLoading css={styles.loader} color={'primary'} />;
  if (!pokemon) return <ErrorPage type={'NOT_FOUND_SEARCH_PAGE'} />;

  return (
    <div css={styles.container}>
      <Image
        css={styles.avatar}
        src={transformPokeAvatar(pokemon.id)}
        width={180}
        height={180}
        alt={'poke'}
        lazy
      />

      <div css={styles.nameContainer}>
        <h1 css={styles.title}>{startCase(camelCase(pokemon.name))}</h1>
        {(location.state as any)?.aliasName && (
          <h2 css={styles.aliasName}>
            ({startCase(camelCase((location.state as any).aliasName))})
          </h2>
        )}
      </div>

      <Space css={styles.space} direction={'vertical'} block>
        <p css={styles.description}>
          {pokeDescription({
            name: pokemon.name,
            generation: pokemon.specy?.generation?.name || '',
            habitat: pokemon.specy?.habitat?.name || 'unknown',
            types: pokemon.types.map(({ type }) => type?.name || ''),
          })}
        </p>

        <div css={styles.infoMargin}>
          <PokeCardInfo
            dataSource={[
              { label: 'Height', value: pokemon.height ? `${pokemon.height}dm` : '-dm' },
              { label: 'Weight', value: pokemon.weight ? `${pokemon.weight}hg` : '-hg' },
              { label: 'Experience', value: pokemon.exp ? `${pokemon.exp}` : '-' },
            ]}
          />
        </div>

        <HeaderTitle title={'Abilities'}>
          <EmblaInfo dataSource={pokemon.abilities.map(({ ability }) => ability?.name || '')} />
        </HeaderTitle>

        <HeaderTitle
          title={'Moves'}
          onSeeAllClick={() => pokemonMoveRef?.current && pokemonMoveRef.current.open(pokemon.id)}
        >
          <PokeMove dataSource={pokemon.moves.map(({ move }) => move?.name || '')} />
        </HeaderTitle>

        <HeaderTitle title={'Base Stats'}>
          <div ref={radarRef} css={styles.radarContainer}>
            <React.Suspense fallback={<SpinLoading css={styles.loaderRadar} color="primary" />}>
              {radarInfViewPort && (
                <Radar
                  {...radarConfig()}
                  data={[
                    { label: 'Sp. Attack', value: pokemon.stats[3].base_stat },
                    { label: 'HP', value: pokemon.stats[0].base_stat },
                    { label: 'Defence', value: pokemon.stats[2].base_stat },
                    { label: 'Sp. Defence', value: pokemon.stats[4].base_stat },
                    { label: 'Attack', value: pokemon.stats[1].base_stat },
                    { label: 'Speed', value: pokemon.stats[5].base_stat },
                  ]}
                />
              )}
            </React.Suspense>
          </div>
        </HeaderTitle>

        <React.Suspense fallback={null}>
          <PokeRecommendation title={'Recommendation'} />
        </React.Suspense>

        <PokemonMove ref={pokemonMoveRef} />
      </Space>
    </div>
  );
};

export default memo(Detail);
