import React, { useState, useRef, memo, forwardRef, useImperativeHandle } from 'react';
import size from 'lodash/size';

import { useCreation } from 'ahooks';
import { useMovesQuery } from '@/codegen/pages';

import { SpinLoading } from 'antd-mobile';
import PokeMove from '@/pages/Pokemon/components/PokeMove';

import * as styles from '@/pages/Pokemon/containers/Detail/styles';

export const FloatingPanel = React.lazy(
  () => /* webpackPrefetch: true */ import('antd-mobile/es/components/floating-panel'),
);

const anchors = [120, window.innerHeight * 0.4, window.innerHeight * 0.8];

const PokemonMove = forwardRef<PokemonMoveRef>(({}, ref) => {
  const floatingPanelRef = useRef<any>();
  const [pokemonId, setPokemonId] = useState<number>();

  const pokemonMovesData = useMovesQuery({
    variables: { id: pokemonId || 0 },
    skip: !pokemonId,
  });

  const pokemonMoves = useCreation(() => {
    return pokemonMovesData.data?.moves;
  }, [pokemonMovesData.data]);

  useImperativeHandle(ref, () => ({
    open: (id: number) => setPokemonId(id),
  }));

  return (
    <>
      {pokemonId ? (
        <React.Suspense fallback={null}>
          <FloatingPanel
            ref={floatingPanelRef}
            css={styles.floating}
            anchors={anchors}
            onHeightChange={(height) => {
              if (height > -110 && floatingPanelRef?.current) {
                floatingPanelRef.current.setHeight(0);
                setPokemonId(0);
              }
            }}
          >
            <div css={styles.wrapperFloating}>
              <div css={styles.innerFloating}>
                {size(pokemonMoves) ? (
                  <PokeMove dataSource={pokemonMoves?.map(({ move }) => move?.name || '') || []} />
                ) : (
                  <SpinLoading css={styles.loader} color="primary" />
                )}
              </div>
            </div>
          </FloatingPanel>
        </React.Suspense>
      ) : null}
    </>
  );
});

export type PokemonMoveRef = {
  open: (id: number) => void;
};

export default memo(PokemonMove);
