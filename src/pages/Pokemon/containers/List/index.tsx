import React, { useRef, memo } from 'react';
import lowerCase from 'lodash/lowerCase';
import size from 'lodash/size';

import { SpinLoading } from 'antd-mobile';

import ErrorPage from '@/components/ErrorPage';
import VirtualGridLayout from '@/components/VirtualGridLayout';

import { useMemoizedFn, useUpdateEffect, useCreation } from 'ahooks';
import { useSearchParams } from 'react-router-dom';
import { usePokemonListQuery } from '@/codegen/pages';

import type { VirtualGridLayoutRef } from '@/components/VirtualGridLayout';

import * as styles from '@/pages/Pokemon/containers/List/styles';

const List = () => {
  const virtualRef = useRef<VirtualGridLayoutRef>(null);

  const [searchValue] = useSearchParams({ search: '' });

  const initialPagination = useCreation(
    () => ({ offset: 0, name: lowerCase(searchValue.get('search') || ''), limit: 20 }),
    [searchValue],
  );

  const pokemonList = usePokemonListQuery({
    variables: initialPagination,
  });

  const fetchMorePokemon = useMemoizedFn(async () => {
    const { data, fetchMore } = pokemonList;
    await fetchMore({
      variables: { ...initialPagination, offset: data?.pokemonV2List.length },
    });
  });

  useUpdateEffect(() => {
    pokemonList.refetch(initialPagination);
    if (virtualRef.current) virtualRef.current.scrollVirtualGrid(0);
  }, [initialPagination, searchValue, virtualRef, pokemonList.refetch]);

  if (pokemonList.loading) {
    return (
      <div css={styles.loader}>
        <SpinLoading color="primary" />
      </div>
    );
  }

  if (!size(pokemonList.data?.pokemonV2List)) {
    return (
      <div css={styles.loader}>
        <ErrorPage type={'NOT_FOUND_SEARCH_PAGE'} />
      </div>
    );
  }

  return (
    <VirtualGridLayout
      ref={virtualRef}
      dataSource={pokemonList.data?.pokemonV2List || []}
      onLoadMore={fetchMorePokemon}
    />
  );
};

export default memo(List);
