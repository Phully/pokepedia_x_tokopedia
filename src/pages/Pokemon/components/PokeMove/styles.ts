import { css } from '@emotion/react';

export const wrapper = css`
  margin-top: 8px;
  margin-bottom: 6px;
`;

export const moveCard = css`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  width: 100%;
  height: 40px;
  background-color: var(--secondary-background-color);
  border-radius: 12px;
  border: 1px solid var(--adm-color-primary);
`;
