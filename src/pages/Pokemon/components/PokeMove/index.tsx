import React, { memo } from 'react';
import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';

import { Grid } from 'antd-mobile';

import * as styles from '@/pages/Pokemon/components/PokeMove/styles';

const PokeMove = ({ dataSource }: PokeMoveProps) => {
  return (
    <Grid css={styles.wrapper} columns={2} gap={18}>
      {dataSource.map((data, index) => {
        if (data) {
          return (
            <Grid.Item key={`${data}-${index}`}>
              <div css={styles.moveCard}>{startCase(camelCase(data))}</div>
            </Grid.Item>
          );
        }
      })}
    </Grid>
  );
};

export type PokeMoveProps = { dataSource: string[] };

export default memo(PokeMove);
