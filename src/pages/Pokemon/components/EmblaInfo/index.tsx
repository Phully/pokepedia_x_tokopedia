import React, { memo } from 'react';
import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';

import Embla from '@/components/Embla';

import * as styles from '@/pages/Pokemon/components/EmblaInfo/styles';

const EmblaInfo = ({ dataSource }: EmblaInfoProps) => {
  return (
    <Embla>
      {dataSource.map((data) => {
        if (data) {
          return (
            <div key={`embla-info-${data}`} css={styles.wrapper}>
              <div css={styles.inner}>{startCase(camelCase(data))}</div>
            </div>
          );
        }
      })}
    </Embla>
  );
};

export type EmblaInfoProps = { dataSource: string[] };

export default memo(EmblaInfo);
