import { css } from '@emotion/react';

export const wrapper = css`
  position: relative;
  padding-left: 16px;
`;

export const inner = css`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 600;
  background: var(--secondary-background-color);
  border-radius: 15px;
  text-align: center;
  width: 140px;
  height: 70px;
  :before {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 2px;
    background-color: #00dbde;
    background-image: linear-gradient(90deg, #00dbde 0%, #fc00ff 100%);
    background-size: 400% 100%;
    border-radius: 15px;
    animation: animation-background-position 1.4s ease infinite;
    content: '';
    mask-composite: exclude;
    mask: linear-gradient(#ffffff 0 0) content-box, linear-gradient(#ffffff 0 0);
    -webkit-mask-composite: xor;
  }
`;
