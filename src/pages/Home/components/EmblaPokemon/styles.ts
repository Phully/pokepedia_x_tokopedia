import { css } from '@emotion/react';

export const container = css`
  height: 285px;
  margin-top: -8px;
`;

export const stateContainer = css`
  height: 285px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: -8px;
`;

export const pokeCardWrapper = css`
  position: relative;
  padding-left: 16px;
`;

export const pokeCardInner = css`
  position: relative;
  width: 175px;
  height: 295px;
  overflow: hidden;
`;
