import React, { memo } from 'react';

import * as styles from '@/pages/Home/components/EmblaPokemon/styles';

const EmblaPokemonStateWrapper = ({ children }: EmblaPokemonStateWrapperProps) => {
  return <div css={styles.stateContainer}>{children}</div>;
};

export type EmblaPokemonStateWrapperProps = {
  children: React.ReactNode;
};

export default memo(EmblaPokemonStateWrapper);
