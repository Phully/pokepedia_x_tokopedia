import React, { memo } from 'react';

import Embla from '@/components/Embla';
import PokeCard from '@/components/PokeCard';

import type { PokemonListQuery } from '@/codegen/graphql';

import * as styles from '@/pages/Home/components/EmblaPokemon/styles';

const EmblaPokemon = ({
  actionTitle,
  catchHistory,
  dataSource,
  shimmer = false,
  onActionClick,
}: EmblaPokemonProps) => {
  return (
    <div css={styles.container}>
      <Embla
        options={{ align: 'center', containScroll: 'trimSnaps', skipSnaps: false, dragFree: true }}
      >
        {dataSource.map((pokemon) => (
          <div key={`pokemon-${pokemon.id}`} css={styles.pokeCardWrapper}>
            <div css={styles.pokeCardInner}>
              <PokeCard
                {...pokemon}
                shimmer={shimmer}
                actionTitle={actionTitle}
                onActionClick={!catchHistory?.[pokemon.id] ? onActionClick : undefined}
              />
            </div>
          </div>
        ))}
      </Embla>
    </div>
  );
};

export type EmblaPokemonProps = {
  catchHistory?: { [key: string]: boolean };
  dataSource: PokemonListQuery['pokemonV2List'][number][];
  shimmer?: boolean;
  actionTitle?: string;
  onActionClick?: (id: number) => void;
};

export default memo(EmblaPokemon);
