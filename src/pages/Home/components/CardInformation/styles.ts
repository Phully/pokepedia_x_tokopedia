import { css } from '@emotion/react';

export const cardInformation = css`
  display: flex;
  align-items: center;
  border-radius: 20px;
  height: 65px;
`;

export const cardInformationWrapper = css`
  display: flex;
  align-items: center;
`;

export const informationWrapper = css`
  display: flex;
  flex-direction: column;
`;

export const title = css`
  font-weight: 200;
`;

export const subtitle = css`
  margin-top: 4px;
  font-size: 12px;
  color: var(--adm-color-primary);
  font-weight: 400;
`;

export const icon = css`
  margin-left: 2px;
  margin-right: 10px;
`;
