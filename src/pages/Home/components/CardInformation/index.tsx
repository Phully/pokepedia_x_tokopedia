import React, { memo, ReactElement } from 'react';

import { Card } from 'antd-mobile';

import * as styles from '@/pages/Home/components/CardInformation/styles';

const CardInformation = ({ title, subtitle, icon }: CardInformationProps) => {
  return (
    <Card css={styles.cardInformation}>
      <div css={styles.cardInformationWrapper}>
        <div css={styles.icon}>{icon}</div>
        <div css={styles.informationWrapper}>
          <span css={styles.title}>{title}</span>
          <span css={styles.subtitle}>{subtitle}</span>
        </div>
      </div>
    </Card>
  );
};

export type CardInformationProps = {
  title: string;
  subtitle: string;
  icon: ReactElement<any, any>;
};

export default memo(CardInformation);
