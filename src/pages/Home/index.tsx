import React from 'react';

export const Main = React.lazy(() => import('@/pages/Home/containers/Main'));

export const MyPokemon = React.lazy(() => import('@/pages/Home/containers/MyPokemon'));

export const Profile = React.lazy(() => import('@/pages/Home/containers/Profile'));
