import request from 'umi-request';

const GET_QUOTE = async () => {
  return request.get(`${process.env.REACT_APP_QUOTE_ENDPOINT}`);
};

export default GET_QUOTE;
