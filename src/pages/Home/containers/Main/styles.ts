import { css } from '@emotion/react';

export const bodyContainer = css`
  display: block;
  width: 100%;
`;

export const space = css`
  --gap-vertical: 26px;
`;
