import React, { memo } from 'react';

import { Space } from 'antd-mobile';

import Quote from '@/pages/Home/containers/Widget/Quote';
import PokeNearby from '@/pages/Home/containers/Widget/PokeNearby';
import PokeInformation from '@/pages/Home/containers/Widget/PokeInformation';
import PokeRecommendation from '@/pages/Home/containers/Widget/PokeRecommendation';
import MyPokemon from '@/pages/Home/containers/Widget/MyPokemon';

import * as styles from '@/pages/Home/containers/Main/styles';

const Main = () => {
  return (
    <div css={styles.bodyContainer}>
      <Space css={styles.space} direction={'vertical'} block>
        <Quote />
        <PokeInformation />
        <PokeNearby title={'Nearby Pokemon'} />
        <PokeRecommendation title={'All Pokemon'} />
        <MyPokemon title={'My Pokemon'} />
      </Space>
    </div>
  );
};

export default memo(Main);
