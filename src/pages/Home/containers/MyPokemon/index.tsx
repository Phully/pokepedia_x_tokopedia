import React, { memo } from 'react';
import size from 'lodash/size';

import { useLockFn } from 'ahooks';
import { useCreation } from 'ahooks';

import useMyPokemonModel from '@/models/useMyPokemonModel';
import VirtualGridLayout from '@/components/VirtualGridLayout';

import sleep from '@/utils/functions/sleep';
import ErrorPage from '@/components/ErrorPage';

const MyPokemon = () => {
  const myPokemonModel = useMyPokemonModel();

  const myPokemon = useCreation(() => {
    return Object.values(myPokemonModel.myPokemon).map((pokemon) => ({
      ...pokemon,
      path: `/pokemon/${pokemon.name}`,
      name: pokemon.aliasName,
    }));
  }, [myPokemonModel.myPokemon]);

  const handleReleasePokemon = useLockFn(async (pokemonId: number) => {
    await sleep(2000);
    const pokemonData = myPokemon.find(({ id }) => id === pokemonId);
    if (pokemonData?.aliasName) myPokemonModel.releasePokemon(pokemonData.aliasName);
  });

  if (!size(myPokemon)) {
    return <ErrorPage type={'EMPTY_POKEMON'} />;
  }

  return (
    <>
      <VirtualGridLayout
        actionTitle={'release'}
        dataSource={myPokemon || []}
        onActionClick={handleReleasePokemon}
      />
    </>
  );
};

export default memo(MyPokemon);
