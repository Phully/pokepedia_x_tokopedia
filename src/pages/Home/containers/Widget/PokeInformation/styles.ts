import { css } from '@emotion/react';

export const cardInformation = css`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  height: 65px;
`;
