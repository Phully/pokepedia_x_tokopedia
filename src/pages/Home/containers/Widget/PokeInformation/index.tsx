import React, { memo } from 'react';
import size from 'lodash/size';

import { Grid, Card, SpinLoading } from 'antd-mobile';

import catchIcon from '@/pages/Home/assets/catching.png';
import pokeballIcon from '@/pages/Home/assets/pokeball.png';
import CardInformation from '@/pages/Home/components/CardInformation';

import { useCreation } from 'ahooks';
import { useTotalPokemonQuery } from '@/codegen/pages';
import useMyPokemonModel from '@/models/useMyPokemonModel';

import * as styles from '@/pages/Home/containers/Widget/PokeInformation/styles';

const PokeInformation = () => {
  const totalPokemon = useTotalPokemonQuery();
  const myPokemonModel = useMyPokemonModel();

  const countPokemon = useCreation(() => {
    return totalPokemon.data?.totalPokemon.aggregate?.count;
  }, [totalPokemon.data]);

  if (totalPokemon.loading) {
    return (
      <Grid columns={2} gap={16}>
        <Grid.Item>
          <Card css={styles.cardInformation}>
            <SpinLoading color="primary" />
          </Card>
        </Grid.Item>
        <Grid.Item>
          <Card css={styles.cardInformation}>
            <SpinLoading color="primary" />
          </Card>
        </Grid.Item>
      </Grid>
    );
  }

  return (
    <Grid columns={2} gap={16}>
      <Grid.Item>
        <CardInformation
          title={'My Pokemon'}
          subtitle={`${size(myPokemonModel.myPokemon)} Pokemon`}
          icon={<img src={pokeballIcon} alt="icon" width={'22px'} height={'22px'} />}
        />
      </Grid.Item>
      <Grid.Item>
        <CardInformation
          title={'Pokedex'}
          subtitle={`${countPokemon ? countPokemon + ' Pokemon' : '(Something error)'}`}
          icon={<img src={catchIcon} alt="icon" width={'26px'} height={'26px'} />}
        />
      </Grid.Item>
    </Grid>
  );
};

export default memo(PokeInformation);
