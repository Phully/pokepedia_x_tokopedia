import React, { memo } from 'react';
import size from 'lodash/size';

import { useCreation } from 'ahooks';
import { useNavigate } from 'react-router-dom';

import HeaderTitle from '@/components/HeaderTitle';
import EmblaPokemonStateWrapper from '@/pages/Home/components/EmblaPokemon/EmblaPokemonStateWrapper';
import EmblaPokemon from '@/pages/Home/components/EmblaPokemon';

import useMyPokemonModel from '@/models/useMyPokemonModel';

const MyPokemon = ({ title }: MyPokemonProps) => {
  const navigate = useNavigate();

  const myPokemonModel = useMyPokemonModel();

  const myPokemon = useCreation(() => {
    return Object.values(myPokemonModel.myPokemon).map((pokemon) => ({
      ...pokemon,
      path: `/pokemon/${pokemon.name}`,
      name: pokemon.aliasName,
    }));
  }, [myPokemonModel.myPokemon]);

  if (!size(myPokemon)) {
    return (
      <HeaderTitle title={title} onSeeAllClick={() => navigate('/home/my-pokemon')}>
        <EmblaPokemonStateWrapper>
          <span>Gotta Catch `Em All!</span>
        </EmblaPokemonStateWrapper>
      </HeaderTitle>
    );
  }

  return (
    <HeaderTitle title={title} onSeeAllClick={() => navigate('/home/my-pokemon')}>
      <EmblaPokemon dataSource={Object.values(myPokemon) || []} shimmer />
    </HeaderTitle>
  );
};

export type MyPokemonProps = {
  title: string;
};

export default memo(MyPokemon);
