import React, { memo, useRef, useEffect, useCallback } from 'react';
import dayjs from 'dayjs';

import { SpinLoading } from 'antd-mobile';

import HeaderTitle from '@/components/HeaderTitle';
import EmblaPokemon from '@/pages/Home/components/EmblaPokemon';
import EmblaPokemonStateWrapper from '@/pages/Home/components/EmblaPokemon/EmblaPokemonStateWrapper';

import { useCookies } from 'react-cookie';
import { usePokemonRecommendationQuery } from '@/codegen/pages';

import useCatchHistory from '@/models/useCatchHistoryModel';
import randomList from '@/utils/functions/random-recommendation';

import type { CatchSystemRef } from '@/pages/Home/containers/Widget/CatchSystem';

const CatchSystem = React.lazy(() => import('@/pages/Home/containers/Widget/CatchSystem'));

const PokeNearby = ({ title }: PokeNearbyProps) => {
  const catchSystemRef = useRef<CatchSystemRef>(null);
  const [cookies, setCookies] = useCookies<string>(['nearby']);
  const { catchHistory } = useCatchHistory();

  const pokemonList = usePokemonRecommendationQuery({
    variables: { offset: 0, limit: 5, id: cookies.nearby?.split('-')?.map(Number) || [] },
    skip: !cookies.nearby,
  });

  const handleCatchPokemon = useCallback(
    (pokemonId: number) => {
      const pokemon = pokemonList.data?.pokemonRecommendation.find(({ id }) => id === pokemonId);
      if (catchSystemRef.current && pokemon) catchSystemRef.current.catch(pokemon);
    },
    [pokemonList.data],
  );

  useEffect(() => {
    if (!cookies.nearby) {
      setCookies('nearby', randomList(1, 649).join('-'), {
        path: '/',
        expires: dayjs().add(10, 'minutes').toDate(),
      });
    }
  }, [cookies, setCookies]);

  if (pokemonList.loading) {
    return (
      <HeaderTitle title={title}>
        <EmblaPokemonStateWrapper>
          <SpinLoading color="primary" />
        </EmblaPokemonStateWrapper>
      </HeaderTitle>
    );
  }

  if (pokemonList.error) {
    return (
      <HeaderTitle title={title}>
        <EmblaPokemonStateWrapper>
          <span>Something went wrong!</span>
        </EmblaPokemonStateWrapper>
      </HeaderTitle>
    );
  }

  return (
    <>
      <HeaderTitle title={title}>
        <EmblaPokemon
          catchHistory={catchHistory}
          dataSource={pokemonList.data?.pokemonRecommendation || []}
          actionTitle={'catch'}
          shimmer={true}
          onActionClick={handleCatchPokemon}
        />
      </HeaderTitle>
      <React.Suspense fallback={null}>
        <CatchSystem ref={catchSystemRef} />
      </React.Suspense>
    </>
  );
};

export type PokeNearbyProps = {
  title: string;
};

export default memo(PokeNearby);
