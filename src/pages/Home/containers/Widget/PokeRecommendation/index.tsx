import React, { memo, useEffect, useRef } from 'react';
import dayjs from 'dayjs';

import { SpinLoading } from 'antd-mobile';

import HeaderTitle from '@/components/HeaderTitle';
import EmblaPokemonStateWrapper from '@/pages/Home/components/EmblaPokemon/EmblaPokemonStateWrapper';
import EmblaPokemon from '@/pages/Home/components/EmblaPokemon';

import { useInViewport } from 'ahooks';
import { useCookies } from 'react-cookie';
import { useNavigate } from 'react-router-dom';
import { usePokemonRecommendationQuery } from '@/codegen/pages';

import randomList from '@/utils/functions/random-recommendation';

const PokeRecommendation = ({ title }: PokeRecommendationProps) => {
  const navigate = useNavigate();
  const pokemonRecommendationRef = useRef(null);
  const [cookies, setCookies] = useCookies<string>(['recommendation']);
  const [recommendationInViewPort] = useInViewport(pokemonRecommendationRef);

  const pokemonList = usePokemonRecommendationQuery({
    variables: { offset: 0, limit: 5, id: cookies.recommendation?.split('-')?.map(Number) || [] },
    skip: !cookies.recommendation || !recommendationInViewPort,
  });

  useEffect(() => {
    if (!cookies.recommendation) {
      setCookies('recommendation', randomList(1, 649).join('-'), {
        path: '/',
        expires: dayjs().add(5, 'minutes').toDate(),
      });
    }
  }, [cookies, setCookies]);

  if (pokemonList.loading) {
    return (
      <div ref={pokemonRecommendationRef}>
        <HeaderTitle title={title} onSeeAllClick={() => navigate('/pokemon-list')}>
          <EmblaPokemonStateWrapper>
            <SpinLoading color="primary" />
          </EmblaPokemonStateWrapper>
        </HeaderTitle>
      </div>
    );
  }

  if (pokemonList.error) {
    return (
      <div ref={pokemonRecommendationRef}>
        <HeaderTitle title={title} onSeeAllClick={() => navigate('/pokemon-list')}>
          <EmblaPokemonStateWrapper>
            <span>Something went wrong!</span>
          </EmblaPokemonStateWrapper>
        </HeaderTitle>
      </div>
    );
  }

  return (
    <div ref={pokemonRecommendationRef}>
      {recommendationInViewPort && (
        <HeaderTitle title={title} onSeeAllClick={() => navigate('/pokemon-list')}>
          <EmblaPokemon dataSource={pokemonList.data?.pokemonRecommendation || []} shimmer />
        </HeaderTitle>
      )}
    </div>
  );
};

export type PokeRecommendationProps = {
  title: string;
};

export default memo(PokeRecommendation);
