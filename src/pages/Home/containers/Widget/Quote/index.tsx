import React, { memo } from 'react';

import { useRequest } from 'ahooks';
import { Card, Divider, SpinLoading } from 'antd-mobile';

import GET_QUOTE from '@/pages/Home/services/quote.service';

import * as styles from '@/pages/Home/containers/Widget/Quote/styles';

const quote = "Always tell the truth. That way, you don't have to remember what you said.";
const author = 'Mark Twain';

const Quote = () => {
  const { data, loading, error } = useRequest(GET_QUOTE, {
    cacheKey: 'cache-quote',
    staleTime: 60000,
  });

  if (loading) {
    return (
      <Card css={styles.card}>
        <SpinLoading color="primary" />
      </Card>
    );
  }

  if (error) {
    return (
      <Card css={styles.card}>
        <span>Something went wrong!</span>
      </Card>
    );
  }

  return (
    <Card css={styles.card}>
      <p css={styles.quote}>{data?.content || quote}</p>
      <Divider contentPosition="center" css={styles.divider}>
        <span>{data?.author || author}</span>
      </Divider>
    </Card>
  );
};

export default memo(Quote);
