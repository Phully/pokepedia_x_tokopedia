import { css } from '@emotion/react';

export const card = css`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  height: 140px;
  padding: 0 32px;
  .adm-card-body {
    padding: 0;
  }
`;

export const divider = css`
  color: var(--adm-color-text);
  border-color: var(--adm-color-text);
  margin: 0 36px;
`;

export const quote = css`
  text-align: center;
  line-height: 22px;
  font-weight: 300;
  margin-bottom: 20px;
  margin-top: 0;
`;
