import React, { memo, useState, forwardRef, useImperativeHandle, useRef } from 'react';
import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';

import { useLockFn, useExternal } from 'ahooks';
import { Modal, Toast, SpinLoading } from 'antd-mobile';

import sleep from '@/utils/functions/sleep';
import randomCatch from '@/utils/functions/random-catch';
import useCatchHistory from '@/models/useCatchHistoryModel';
import useMyPokemonModel from '@/models/useMyPokemonModel';

import type { PokemonListQuery } from '@/codegen/graphql';

import * as styles from '@/pages/Home/containers/Widget/CatchSystem/styles';

const Lottie = React.lazy(() => import('react-lottie'));
const CatchForm = React.lazy(() => import('@/pages/Home/containers/Widget/CatchSystem/CatchForm'));

const CatchSystem = forwardRef<CatchSystemRef>(({}, ref) => {
  const refPikachu = useRef('');
  const [form, setForm] = useState(false);
  const [initialCatch, setInitialCatch] = useState(false);
  const [pokemon, setPokemon] = useState<PokemonListQuery['pokemonV2List'][number]>();
  const { myPokemon, addPokemon } = useMyPokemonModel();
  const { addToCatchHistory } = useCatchHistory();

  const pikachuStatus = useExternal(refPikachu.current ? refPikachu.current : '', {
    js: { async: true },
  });

  const handleInitialCatchPokemon = useLockFn(
    async (pokemonData: PokemonListQuery['pokemonV2List'][number]) => {
      const isCatch = randomCatch();
      refPikachu.current = 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu-lottie.js';
      setInitialCatch(true);
      setPokemon(pokemonData);
      await sleep(1500);
      if (isCatch) {
        setForm(true);
      } else {
        const content = `${startCase(camelCase(pokemonData.name))} run away!`;
        Toast.show({ position: 'bottom', content: content });
        addToCatchHistory(pokemonData.id);
      }
      setInitialCatch(false);
    },
  );

  const handleSuccessCatchPokemon = useLockFn(
    async (name: any, pokemonData: PokemonListQuery['pokemonV2List'][number]) => {
      const content = `${startCase(camelCase(pokemonData.name))} catch success!`;
      addPokemon(name, pokemonData);
      addToCatchHistory(pokemonData.id);
      Toast.show({ position: 'bottom', content: content });
      setForm(false);
    },
  );

  useImperativeHandle(ref, () => ({
    catch: async (pokemonData) => {
      await handleInitialCatchPokemon(pokemonData);
    },
  }));

  return (
    <>
      {initialCatch && (
        <Modal
          visible={initialCatch}
          css={styles.loaderPikachuModal}
          content={
            <React.Suspense fallback={<SpinLoading css={styles.loaderLottie} color="primary" />}>
              {pikachuStatus === 'ready' ? (
                <Lottie
                  height={280}
                  width={280}
                  isClickToPauseDisabled={true}
                  options={{
                    animationData: (window as any).pikachuLottie,
                    autoplay: true,
                    loop: true,
                  }}
                  eventListeners={[]}
                />
              ) : (
                <SpinLoading css={styles.loaderLottie} color="primary" />
              )}
            </React.Suspense>
          }
        />
      )}

      {form && (
        <Modal
          visible={form}
          css={styles.formModal}
          content={
            <React.Suspense fallback={<SpinLoading css={styles.loaderForm} color="primary" />}>
              <CatchForm
                pokemon={pokemon}
                myPokemon={myPokemon}
                onCatchSuccess={handleSuccessCatchPokemon}
              />
            </React.Suspense>
          }
        />
      )}
    </>
  );
});

export type CatchSystemRef = {
  catch: (pokemonData: PokemonListQuery['pokemonV2List'][number]) => void;
};

export default memo(CatchSystem);
