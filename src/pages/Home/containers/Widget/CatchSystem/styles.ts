import { css } from '@emotion/react';

export const loaderPikachuModal = css`
  display: flex;
  align-items: center;
  border-radius: 20px;
  height: 65px;
  .adm-modal-body {
    background-color: transparent;
  }
`;

export const formModal = css`
  display: flex;
  align-items: center;
  border-radius: 20px;
  --border-top: 0;
  --border-bottom: 0;

  .adm-modal-wrap {
    width: 85% !important;
    min-width: 85% !important;
    max-width: 85% !important;
  }

  .adm-modal-content {
    padding: 0;
  }

  .adm-modal-body {
    min-height: 185px;
    padding: 40px 20px 20px;
    background-color: var(--primary-background-color);
    border: none !important;
    border-radius: 20px;
    :before {
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      padding: 2px;
      background-color: #00dbde;
      background-image: linear-gradient(90deg, #00dbde 0%, #fc00ff 100%);
      background-size: 400% 100%;
      border-radius: 25px;
      animation: animation-background-position 1.4s ease infinite;
      content: '';
      mask-composite: exclude;
      mask: linear-gradient(#fff 0 0) content-box, linear-gradient(#fff 0 0);
      -webkit-mask-composite: xor;
    }
  }

  .adm-list-body-inner {
    background-color: var(--primary-background-color);
  }

  .adm-form-footer {
    margin-top: 10px;
    margin-right: 20px;
    margin-left: 20px;
    padding: 0 !important;
  }

  .adm-modal-footer-empty {
    display: none;
  }

  .adm-list-default .adm-list-body {
    border-top: 0 !important;
    border-bottom: 0 !important;
  }

  .adm-form-item-label {
    color: #ffffffcc !important;
  }

  .adm-modal-content {
    overflow: hidden;
  }
`;

export const icon = css`
  position: absolute;
  left: 0;
  right: 0;
  top: -80px;
  margin: auto;
  .adm-image-tip {
    background-color: var(--primary-background-color);
    border-radius: 30px;
    box-shadow: inset 0 0 0 1px var(--adm-color-primary);
  }
`;

export const loaderLottie = css`
  margin: auto;
`;

export const loaderForm = css`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
`;
