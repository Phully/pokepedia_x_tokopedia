import React from 'react';

import { useLockFn } from 'ahooks';
import { Button, Image, Input } from 'antd-mobile';

import Form from 'antd-mobile/es/components/form';

import transformPokeAvatar from '@/utils/functions/poke-avatar';

import * as styles from '@/pages/Home/containers/Widget/CatchSystem/styles';

import type { PokemonListQuery } from '@/codegen/graphql';
import type { MyPokemonType } from '@/models/useMyPokemonModel';

const CatchForm = ({ pokemon, myPokemon, onCatchSuccess }: CatchFormProps) => {
  const [formRef] = Form.useForm();

  const validatePokemonName = useLockFn(async (formData: any) => {
    if (formData?.name && pokemon) {
      if (myPokemon?.[formData.name]) {
        formRef.setFields([{ name: 'name', errors: ["I don't like that name."] }]);
      } else {
        onCatchSuccess(formData.name, pokemon);
        formRef.resetFields();
      }
    }
  });

  return (
    <>
      <Image
        css={styles.icon}
        src={transformPokeAvatar(pokemon?.id || 0)}
        width={120}
        height={120}
        alt={'poke'}
        lazy
      />
      <Form
        form={formRef}
        name={'pokemon-name-form'}
        onFinish={validatePokemonName}
        footer={
          <Button type={'submit'} color={'primary'} size={'middle'} shape={'rounded'} block>
            catch
          </Button>
        }
        preserve={false}
      >
        <Form.Item
          name="name"
          label="Give me a Name?"
          rules={[
            { required: true, message: 'Please call me something.' },
            { min: 3, message: 'I like name more than 3 character.' },
            { max: 10, message: 'Please call me something.' },
          ]}
        >
          <Input placeholder="example : magicarp" maxLength={10} />
        </Form.Item>
      </Form>
    </>
  );
};

export type CatchFormProps = {
  pokemon?: PokemonListQuery['pokemonV2List'][number];
  myPokemon: MyPokemonType;
  onCatchSuccess: (
    pokemonName: string,
    pokemonData: PokemonListQuery['pokemonV2List'][number],
  ) => void;
};

export default CatchForm;
