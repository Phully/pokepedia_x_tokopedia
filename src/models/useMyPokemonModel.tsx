import { useCallback, useState } from 'react';
import { useLocalStorageState } from 'ahooks';
import { createModel } from 'hox';

import omit from 'lodash/omit';

import type { PokemonListQuery } from '@/codegen/graphql';

function useMyPokemonModel() {
  const [myPokemonStorage, setMyPokemonStorage] = useLocalStorageState<MyPokemonType>(
    'my-pokemon',
    { defaultValue: {} },
  );

  const [myPokemon, setMyPokemon] = useState<MyPokemonType>(myPokemonStorage);

  const addPokemon = useCallback(
    (pokemonName: string, pokemon: PokemonListQuery['pokemonV2List'][number]) => {
      const pokemonData = { ...myPokemon, [pokemonName]: { ...pokemon, aliasName: pokemonName } };
      setMyPokemon(pokemonData);
      setMyPokemonStorage(pokemonData);
    },
    [myPokemon, setMyPokemon, setMyPokemonStorage],
  );

  const releasePokemon = useCallback(
    (pokemonName: string) => {
      const newListPokemon = omit(myPokemon, pokemonName);
      setMyPokemon({ ...newListPokemon });
      setMyPokemonStorage({ ...newListPokemon });
    },
    [myPokemon, setMyPokemon, setMyPokemonStorage],
  );

  return {
    myPokemon,
    addPokemon,
    releasePokemon,
  };
}

export type MyPokemonType = {
  [key: string]: PokemonListQuery['pokemonV2List'][number] & { aliasName: string };
};

export default createModel(useMyPokemonModel);
