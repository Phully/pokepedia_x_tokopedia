import { useCallback, useState } from 'react';
import { useLocalStorageState } from 'ahooks';
import { createModel } from 'hox';

const useCatchHistory = () => {
  const [catchHistoryStorage, setCatchHistoryStorage] = useLocalStorageState<CatchHistoryType>(
    'catch-history',
    { defaultValue: {} },
  );

  const [catchHistory, setCatchHistory] = useState<CatchHistoryType>({
    ...catchHistoryStorage,
  });

  const addToCatchHistory = useCallback(
    (id: number) => {
      const catchHistoryData = { ...catchHistory, [id]: true };
      setCatchHistory({ ...catchHistory, [id]: true });
      setCatchHistoryStorage(catchHistoryData);
    },
    [catchHistory, setCatchHistory, setCatchHistoryStorage],
  );

  const clearCatchHistory = useCallback(() => {
    setCatchHistory({});
    setCatchHistoryStorage({});
  }, [setCatchHistory, setCatchHistoryStorage]);

  return {
    catchHistory,
    addToCatchHistory,
    clearCatchHistory,
  };
};

type CatchHistoryType = {
  [key: number]: boolean;
};

export default createModel(useCatchHistory);
