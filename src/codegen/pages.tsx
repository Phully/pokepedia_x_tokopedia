import * as Types from '@/codegen/graphql';

import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
const defaultOptions = {} as const;

export const PokemonRecommendationDocument = gql`
    query pokemonRecommendation($limit: Int!, $offset: Int!, $id: [Int!]) {
  pokemonRecommendation: pokemon_v2_pokemon(
    limit: $limit
    offset: $offset
    where: {id: {_in: $id}}
  ) {
    id
    name
    types: pokemon_v2_pokemontypes {
      type: pokemon_v2_type {
        name
      }
    }
    height
    weight
    stats: pokemon_v2_pokemonstats {
      base_stat
      stat: pokemon_v2_stat {
        name
      }
    }
  }
}
    `;

/**
 * __usePokemonRecommendationQuery__
 *
 * To run a query within a React component, call `usePokemonRecommendationQuery` and pass it any options that fit your needs.
 * When your component renders, `usePokemonRecommendationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePokemonRecommendationQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePokemonRecommendationQuery(baseOptions: Apollo.QueryHookOptions<Types.PokemonRecommendationQuery, Types.PokemonRecommendationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Types.PokemonRecommendationQuery, Types.PokemonRecommendationQueryVariables>(PokemonRecommendationDocument, options);
      }
export function usePokemonRecommendationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Types.PokemonRecommendationQuery, Types.PokemonRecommendationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Types.PokemonRecommendationQuery, Types.PokemonRecommendationQueryVariables>(PokemonRecommendationDocument, options);
        }
export type PokemonRecommendationQueryHookResult = ReturnType<typeof usePokemonRecommendationQuery>;
export type PokemonRecommendationLazyQueryHookResult = ReturnType<typeof usePokemonRecommendationLazyQuery>;
export type PokemonRecommendationQueryResult = Apollo.QueryResult<Types.PokemonRecommendationQuery, Types.PokemonRecommendationQueryVariables>;
export const TotalPokemonDocument = gql`
    query totalPokemon {
  totalPokemon: pokemon_v2_pokemon_aggregate {
    aggregate {
      count
    }
  }
}
    `;

/**
 * __useTotalPokemonQuery__
 *
 * To run a query within a React component, call `useTotalPokemonQuery` and pass it any options that fit your needs.
 * When your component renders, `useTotalPokemonQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTotalPokemonQuery({
 *   variables: {
 *   },
 * });
 */
export function useTotalPokemonQuery(baseOptions?: Apollo.QueryHookOptions<Types.TotalPokemonQuery, Types.TotalPokemonQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Types.TotalPokemonQuery, Types.TotalPokemonQueryVariables>(TotalPokemonDocument, options);
      }
export function useTotalPokemonLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Types.TotalPokemonQuery, Types.TotalPokemonQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Types.TotalPokemonQuery, Types.TotalPokemonQueryVariables>(TotalPokemonDocument, options);
        }
export type TotalPokemonQueryHookResult = ReturnType<typeof useTotalPokemonQuery>;
export type TotalPokemonLazyQueryHookResult = ReturnType<typeof useTotalPokemonLazyQuery>;
export type TotalPokemonQueryResult = Apollo.QueryResult<Types.TotalPokemonQuery, Types.TotalPokemonQueryVariables>;
export const PokemonDetailDocument = gql`
    query PokemonDetail($name: String!) {
  pokemonDetail: pokemon_v2_pokemon(where: {name: {_like: $name}}, limit: 1) {
    id
    name
    types: pokemon_v2_pokemontypes {
      type: pokemon_v2_type {
        name
      }
    }
    height
    weight
    stats: pokemon_v2_pokemonstats {
      base_stat
      stat: pokemon_v2_stat {
        name
      }
    }
    exp: base_experience
    abilities: pokemon_v2_pokemonabilities {
      ability: pokemon_v2_ability {
        name
      }
    }
    moves: pokemon_v2_pokemonmoves(limit: 2, offset: 0) {
      move: pokemon_v2_move {
        name
      }
    }
    specy: pokemon_v2_pokemonspecy {
      generation: pokemon_v2_generation {
        name
      }
      habitat: pokemon_v2_pokemonhabitat {
        name
      }
    }
  }
}
    `;

/**
 * __usePokemonDetailQuery__
 *
 * To run a query within a React component, call `usePokemonDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `usePokemonDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePokemonDetailQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function usePokemonDetailQuery(baseOptions: Apollo.QueryHookOptions<Types.PokemonDetailQuery, Types.PokemonDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Types.PokemonDetailQuery, Types.PokemonDetailQueryVariables>(PokemonDetailDocument, options);
      }
export function usePokemonDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Types.PokemonDetailQuery, Types.PokemonDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Types.PokemonDetailQuery, Types.PokemonDetailQueryVariables>(PokemonDetailDocument, options);
        }
export type PokemonDetailQueryHookResult = ReturnType<typeof usePokemonDetailQuery>;
export type PokemonDetailLazyQueryHookResult = ReturnType<typeof usePokemonDetailLazyQuery>;
export type PokemonDetailQueryResult = Apollo.QueryResult<Types.PokemonDetailQuery, Types.PokemonDetailQueryVariables>;
export const MovesDocument = gql`
    query Moves($id: Int!) {
  moves: pokemon_v2_pokemonmove(where: {pokemon_id: {_eq: $id}}) {
    move: pokemon_v2_move {
      name
    }
  }
}
    `;

/**
 * __useMovesQuery__
 *
 * To run a query within a React component, call `useMovesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMovesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMovesQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMovesQuery(baseOptions: Apollo.QueryHookOptions<Types.MovesQuery, Types.MovesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Types.MovesQuery, Types.MovesQueryVariables>(MovesDocument, options);
      }
export function useMovesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Types.MovesQuery, Types.MovesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Types.MovesQuery, Types.MovesQueryVariables>(MovesDocument, options);
        }
export type MovesQueryHookResult = ReturnType<typeof useMovesQuery>;
export type MovesLazyQueryHookResult = ReturnType<typeof useMovesLazyQuery>;
export type MovesQueryResult = Apollo.QueryResult<Types.MovesQuery, Types.MovesQueryVariables>;
export const PokemonListDocument = gql`
    query PokemonList($limit: Int!, $offset: Int!, $name: String!) {
  pokemonV2List: pokemon_v2_pokemon(
    limit: $limit
    offset: $offset
    where: {name: {_regex: $name}}
  ) {
    id
    name
    types: pokemon_v2_pokemontypes {
      type: pokemon_v2_type {
        name
      }
    }
    height
    weight
    stats: pokemon_v2_pokemonstats {
      base_stat
      stat: pokemon_v2_stat {
        name
      }
    }
  }
}
    `;

/**
 * __usePokemonListQuery__
 *
 * To run a query within a React component, call `usePokemonListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePokemonListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePokemonListQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      name: // value for 'name'
 *   },
 * });
 */
export function usePokemonListQuery(baseOptions: Apollo.QueryHookOptions<Types.PokemonListQuery, Types.PokemonListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Types.PokemonListQuery, Types.PokemonListQueryVariables>(PokemonListDocument, options);
      }
export function usePokemonListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Types.PokemonListQuery, Types.PokemonListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Types.PokemonListQuery, Types.PokemonListQueryVariables>(PokemonListDocument, options);
        }
export type PokemonListQueryHookResult = ReturnType<typeof usePokemonListQuery>;
export type PokemonListLazyQueryHookResult = ReturnType<typeof usePokemonListLazyQuery>;
export type PokemonListQueryResult = Apollo.QueryResult<Types.PokemonListQuery, Types.PokemonListQueryVariables>;