import { css } from '@emotion/react';

export const global = css`
  :root:root {
    --adm-color-primary: #f7ba15;
    --primary-background-color: #2c3151;
    --secondary-background-color: #353d64;
    --adm-color-text: #ffffff;
    --adm-color-text-weak: #b8b0b1;
    --adm-font-family: 'Nunito', -apple-system, blinkmacsystemfont, 'Helvetica Neue', helvetica,
      segoe ui, arial, roboto, 'PingFang SC', 'miui', 'Hiragino Sans GB', 'Microsoft Yahei',
      sans-serif;
  }

  ::-webkit-scrollbar {
    width: 6px;
  }

  ::-webkit-scrollbar-track {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
  }

  @keyframes animation-background-position {
    0% {
      background-position: 100% 50%;
    }
    100% {
      background-position: 0 50%;
    }
  }

  html {
    background-color: var(--primary-background-color);
  }

  body {
    margin: 0;
  }

  #root {
    max-width: 500px;
    margin: 0 auto;
  }

  .adm-card {
    background-color: var(--secondary-background-color) !important;
  }

  .main-loader {
    height: 100vh;
  }
`;

export default global;
