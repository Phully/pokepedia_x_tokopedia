import { ApolloClient, InMemoryCache } from '@apollo/client';
import { offsetLimitPagination } from '@apollo/client/utilities';

const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        pokemon_v2_pokemon: offsetLimitPagination(['where']),
      },
    },
  },
});

const client = new ApolloClient({
  uri: process.env.REACT_APP_POKEAPI_ENDPOINT,
  cache: cache,
});

export default client;
