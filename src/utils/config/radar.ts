const radarConfig = () => {
  return {
    meta: { value: { min: 0, nice: true } },
    xField: 'label',
    yField: 'value',
    xAxis: {
      line: null,
      tickLine: null,
      label: { style: { fill: '#ffffffB3' } },
      grid: { line: { style: { lineDash: null } } },
    },
    yAxis: {
      line: null,
      label: false,
      tickLine: null,
      grid: {
        line: { type: 'line', style: { lineDash: null } },
        alternateColor: 'rgba(0, 0, 0, 0.04)',
      },
    },
    color: '#f7ba15',
    point: { size: 2 },
    padding: [0, 45, 0, 45],
    area: {},
  };
};

export default radarConfig;
