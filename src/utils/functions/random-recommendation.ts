type randomListType = (min: number, max: number, length?: number) => number[];

const randomList: randomListType = (min, max, length = 5) => {
  return Array(length)
    .fill(undefined)
    .map(() => Math.floor(Math.random() * max + min));
};

export default randomList;
