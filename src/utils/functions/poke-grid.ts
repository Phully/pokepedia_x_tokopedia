const transformPokeGrid = <T>(arrayInput: T[], grid = 2) => {
  const copyArrayInput = [...arrayInput];
  const matrix: T[][] = [];
  while (copyArrayInput.length) matrix.push(copyArrayInput.splice(0, grid));
  return matrix;
};

export default transformPokeGrid;
