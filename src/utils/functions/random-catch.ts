type randomListType = () => boolean;

const randomCatch: randomListType = () => {
  return Math.random() >= 0.5;
};

export default randomCatch;
