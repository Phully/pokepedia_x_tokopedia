import camelCase from 'lodash/camelCase';
import startCase from 'lodash/startCase';

type PokeDescriptionType = {
  name: string;
  generation: string;
  habitat: string;
  types: string[];
};

const pokeDescription = ({ name, generation, habitat, types }: PokeDescriptionType) => {
  const caseName = startCase(camelCase(name));
  const caseGeneration = startCase(camelCase(generation));
  const caseHabitat = habitat;
  const typesCase = startCase(camelCase(types.join('/')));
  return `${caseName} is a ${typesCase} type pokemon introduced in ${caseGeneration}, It is often in its habitat known as ${caseHabitat}`;
};

export default pokeDescription;
