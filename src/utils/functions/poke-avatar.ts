type TransformPokeAvatarType = (id: number) => string;

const transformPokeAvatar: TransformPokeAvatarType = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id}.svg`;
};

export default transformPokeAvatar;
