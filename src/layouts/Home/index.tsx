import React, { memo } from 'react';

import { NavBar } from 'antd-mobile';
import { SearchOutline, KoubeiOutline } from 'antd-mobile-icons';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';

import Footer from '@/layouts/Home/Footer';
import HOME_MAPPER from '@/layouts/Home/home-mapper';

import * as styles from '@/layouts/Home/styles';

const Home = () => {
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <div css={styles.app}>
      <div css={styles.header}>
        <NavBar
          css={styles.navbar}
          left={
            <div css={styles.logoContainer}>
              <KoubeiOutline css={`${styles.logo}`} />
              <span css={styles.logoTitle}>
                {HOME_MAPPER?.[location.pathname]?.title || 'Pokepedia'}
              </span>
            </div>
          }
          right={
            <div>
              <SearchOutline css={styles.search} onClick={() => navigate('/pokemon-list')} />
            </div>
          }
          back={null}
        />
      </div>
      <div css={styles.body}>
        <Outlet />
      </div>
      <div css={styles.footer}>
        <Footer />
      </div>
    </div>
  );
};

export default memo(Home);
