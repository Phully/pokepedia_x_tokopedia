import React from 'react';

import { AppOutline, FaceRecognitionOutline, SystemQRcodeOutline } from 'antd-mobile-icons';

import * as styles from '@/layouts/Home/styles';

export type HomeNavigationType = {
  key: string;
  icon: any;
  title: string;
};

const HOME_MAPPER: Record<string, HomeNavigationType> = {
  '/home/widget': {
    key: '/home/widget',
    icon: <AppOutline css={styles.tabBarIcon} />,
    title: 'Pokepedia',
  },
  '/home/my-pokemon': {
    key: '/home/my-pokemon',
    icon: <SystemQRcodeOutline css={styles.tabBarIcon} />,
    title: 'My Pokemon',
  },
  '/home/profile': {
    key: '/home/profile',
    icon: <FaceRecognitionOutline css={styles.tabBarIcon} />,
    title: 'Profile',
  },
};

export default HOME_MAPPER;
