import { css } from '@emotion/react';

export const app = css`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

export const header = css`
  flex: 0;
  position: sticky;
  top: 0;
  background-color: var(--primary-background-color) !important;
  z-index: 4;
`;

export const navbar = css`
  padding: 8px 16px;
  .adm-nav-bar-title {
    padding-right: 0;
    padding-left: 12px;
  }
  .adm-nav-bar-back {
    margin-top: 4px;
    margin-right: 0;
  }
`;

export const body = css`
  position: relative;
  display: flex;
  flex: 1;
  align-items: flex-start;
  justify-content: center;
  padding: 0 16px;
  overflow: auto;
`;

export const footer = css`
  flex: 0;
  position: sticky;
  bottom: 0;
  background-color: var(--primary-background-color) !important;
`;

export const logoContainer = css`
  display: flex;
  align-items: center;
  font-size: 22px;
`;

export const logo = css`
  margin-right: 6px;
`;

export const logoTitle = css`
  font-weight: 900;
  font-size: 18px;
  margin-left: 10px;
`;

export const search = css`
  font-size: 22px;
`;

export const tabBarIcon = css`
  font-size: 22px;
`;

export const tabBar = css`
  .adm-tab-bar-item {
    color: var(--adm-color-text);
  }

  .adm-tab-bar-item-active {
    color: var(--adm-color-primary) !important;
  }
`;
