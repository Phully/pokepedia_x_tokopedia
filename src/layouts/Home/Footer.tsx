import React from 'react';

import { TabBar } from 'antd-mobile';
import { useLocation, useNavigate } from 'react-router-dom';

import HOME_MAPPER from '@/layouts/Home/home-mapper';

import * as styles from '@/layouts/Home/styles';

const Footer = () => {
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <TabBar activeKey={location.pathname} css={styles.tabBar} onChange={(key) => navigate(key)}>
      {Object.values(HOME_MAPPER).map((item) => (
        <TabBar.Item key={item.key} icon={item.icon} />
      ))}
    </TabBar>
  );
};

export default Footer;
