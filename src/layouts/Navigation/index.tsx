import React, { memo, useRef, useEffect } from 'react';

import { NavBar } from 'antd-mobile';
import { Outlet, useLocation, useNavigate, useSearchParams } from 'react-router-dom';

import * as styles from '@/layouts/Navigation/styles';

const Navigation = () => {
  const bodyRef = useRef<HTMLDivElement>(null);
  const location = useLocation();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams({ 'navbar-title': '' });

  useEffect(() => {
    if (bodyRef.current) bodyRef.current.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <div css={styles.app}>
      <div css={styles.header}>
        <NavBar css={[styles.navbar]} onBack={() => navigate(-1)}>
          {searchParams.get('navbar-title')}
        </NavBar>
      </div>
      <div ref={bodyRef} css={styles.body}>
        <Outlet />
      </div>
    </div>
  );
};

export default memo(Navigation);
