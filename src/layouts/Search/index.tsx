import React, { useState, memo } from 'react';

import { NavBar, SearchBar } from 'antd-mobile';

import { useUpdateEffect, useDebounce } from 'ahooks';
import { useSearchParams, Outlet, useNavigate } from 'react-router-dom';

import * as styles from '@/layouts/Search/styles';

const Search = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams({ search: '' });
  const [searchValue, setSearchValue] = useState(searchParams.get('search'));

  const debouncedValue = useDebounce(searchValue, { wait: 650 });

  useUpdateEffect(() => {
    setSearchParams({ search: debouncedValue?.trim() || '' });
  }, [debouncedValue]);

  return (
    <div css={styles.app}>
      <div css={styles.header}>
        <NavBar css={[styles.navbar, styles.searchBar]} onBack={() => navigate(-1)}>
          <SearchBar
            value={searchValue || ''}
            placeholder={'Search Pokemon'}
            cancelText={'Cancel'}
            showCancelButton={true}
            onChange={(search) => setSearchValue(search)}
          />
        </NavBar>
      </div>
      <div css={styles.body}>
        <Outlet />
      </div>
    </div>
  );
};

export type SearchLayoutProps = { onRefetch: () => Promise<void> };

export default memo(Search);
