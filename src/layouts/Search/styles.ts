import { css } from '@emotion/react';

export const app = css`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

export const header = css`
  flex: 0;
  position: sticky;
  top: 0;
  background-color: var(--primary-background-color) !important;
  z-index: 4;
`;

export const navbar = css`
  padding: 8px 20px 8px 16px;
  .adm-nav-bar-left {
    flex: none;
  }

  .adm-nav-bar-title {
    padding-right: 0;
    padding-left: 12px;
  }

  .adm-nav-bar-right {
    display: none;
  }

  .adm-nav-bar-back {
    margin-top: 4px;
    margin-right: 0;
  }
`;

export const searchBar = css`
  .adm-search-bar {
    --height: 36px;
    --background: var(--secondary-background-color);
  }
  .adm-search-bar-active .adm-search-bar-input-box {
    background: var(--primary-background-color) !important;
    border-color: var(--adm-color-primary);
  }
`;

export const body = css`
  position: relative;
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
`;
