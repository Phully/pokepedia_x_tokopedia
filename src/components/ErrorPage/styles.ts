import { css } from '@emotion/react';

export const container = css`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 88%;
  width: 100%;
`;

export const info = css`
  margin-top: -20px;
  text-align: center;
`;

export const title = css`
  font-size: 20px;
  color: var(--adm-color-text);
  line-height: 1.4;
  font-weight: normal;
  margin: 0;
`;

export const description = css`
  color: var(--adm-color-text);
  line-height: 1.4;
  font-weight: normal;
  margin: 8px 0 0;
`;

export const action = css`
  color: var(--primary-background-color);
`;
