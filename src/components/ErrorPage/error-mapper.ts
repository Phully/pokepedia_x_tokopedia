export const ERROR_PAGE_MAPPER = {
  UNDER_CONSTRUCTION: {
    image: 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu.webp',
    title: 'Under Construction',
    description: 'We are currently working on awesome new site.',
    actionTitle: '',
  },
  SOMETHING_ERROR: {
    image: 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu.webp',
    title: 'Something Went Wrong!',
    description: `Brace yourself till we get the error fixed.`,
    actionTitle: 'Go back to Home',
  },
  NOT_FOUND_PAGE: {
    image: 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu.webp',
    title: 'Whoops!',
    description: `We couldn't connect you to the page.`,
    actionTitle: 'Go back to Home',
  },
  EMPTY_POKEMON: {
    image: 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu.webp',
    title: 'Gotta Catch `Em All!',
    description: `Catch your pokemon in nearby menu.`,
    actionTitle: '',
  },
  NOT_FOUND_SEARCH_PAGE: {
    image: 'https://cdn.jsdelivr.net/gh/conioX/pikachu/pikachu.webp',
    title: 'Whoops!',
    description: `Try with different keyword.`,
    actionTitle: 'Go back to Home',
  },
};
