import React, { memo } from 'react';

import { Button } from 'antd-mobile';

import { ERROR_PAGE_MAPPER } from '@/components/ErrorPage/error-mapper';

import * as styles from '@/components/ErrorPage/styles';

const ErrorPage = ({ type, actionTitle, onActionClick }: ErrorPageProps) => {
  return (
    <div css={styles.container}>
      <img src={ERROR_PAGE_MAPPER[type].image} width={165} height={165} alt={'error-page'} />
      <div css={styles.info}>
        <h3 css={styles.title}>{ERROR_PAGE_MAPPER[type].title}</h3>
        <p css={styles.description}>{ERROR_PAGE_MAPPER[type].description}</p>
      </div>
      {onActionClick && (
        <Button css={styles.action} shape={'rounded'} color={'primary'}>
          {actionTitle}
        </Button>
      )}
    </div>
  );
};

export type ErrorPageProps = {
  type:
    | 'UNDER_CONSTRUCTION'
    | 'SOMETHING_ERROR'
    | 'NOT_FOUND_PAGE'
    | 'EMPTY_POKEMON'
    | 'NOT_FOUND_SEARCH_PAGE';
  actionTitle?: string;
  onActionClick?: () => void;
};

export default memo(ErrorPage);
