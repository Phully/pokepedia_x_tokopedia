import { css } from '@emotion/react';

export const cardWrapper = css`
  position: relative;
  min-height: 240px;
  min-width: 160px;
  margin-top: 30px;
  border-radius: 25px;
  box-sizing: border-box;
`;

export const cardWrapperShimmer = css`
  :before {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 2px;
    background: linear-gradient(90deg, #fad961 0%, #f76b1c 100%);
    background-size: 400% 100%;
    border-radius: 25px;
    animation: animation-background-position 1.4s ease infinite;
    content: '';
    mask-composite: exclude;
    mask: linear-gradient(#fff 0 0) content-box, linear-gradient(#fff 0 0);
    -webkit-mask-composite: xor;
  }
`;

export const infoWrapper = css`
  display: block;
  text-align: center;
  margin: 75px 0 18px;
`;

export const avatar = css`
  position: absolute;
  top: -25px;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  .adm-image {
    margin: auto;
  }
  .adm-image-tip {
    background-color: var(--primary-background-color);
    border-radius: 30px;
    box-shadow: inset 0 0 0 1px var(--adm-color-primary);
  }
`;

export const name = css`
  font-size: 16px;
  text-transform: capitalize;
`;

export const infoInner = css`
  margin-bottom: 16px;
`;

export const labelContainer = css`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const labelInfo = css`
  font-size: 12px;
  margin: 0 0 4px;
  font-weight: 200;
`;

export const valueInfo = css`
  font-size: 13px;
  color: var(--adm-color-primary);
`;

export const action = css`
  position: absolute;
  color: var(--primary-background-color);
  font-weight: 600;
  left: 0;
  right: 0;
  margin: 0 auto;
  width: fit-content;
  bottom: -12px;
  opacity: 1 !important;
`;

export const nameContainer = css`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 0 20px;
`;
