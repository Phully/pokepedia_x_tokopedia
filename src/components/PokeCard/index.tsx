import React, { memo, useState } from 'react';

import { useLockFn } from 'ahooks';
import { useLocation, useNavigate } from 'react-router-dom';
import { Card, Image, Button } from 'antd-mobile';

import PokeCardInfo from '@/components/PokeCard/PokeCardInfo';
import transformPokeAvatar from '@/utils/functions/poke-avatar';

import type { PokemonListQuery } from '@/codegen/graphql';

import * as styles from '@/components/PokeCard/styles';

const PokeCard = ({
  id,
  name,
  height,
  weight,
  stats,
  actionTitle,
  shimmer = false,
  path,
  aliasName,
  onActionClick,
}: PokeCardProps) => {
  const location = useLocation();
  const navigate = useNavigate();
  const [action, setAction] = useState(false);

  const handleOnCardClick = useLockFn(async (event) => {
    event.stopPropagation();
    if (location.pathname !== `/pokemon/${name}`) {
      const pokemonNumber = `#${String(id).padStart(3, '0')}`;
      const navbarTitle = `?navbar-title=${encodeURIComponent(pokemonNumber)}`;
      navigate(
        { pathname: path || `/pokemon/${name}`, search: navbarTitle },
        { state: { aliasName: aliasName } },
      );
    }
  });

  const handleActionClick = useLockFn(async (event) => {
    event.stopPropagation();
    if (onActionClick) {
      setAction(true);
      await onActionClick(id);
      setAction(false);
    }
  });

  return (
    <Card
      css={[styles.cardWrapper, shimmer && styles.cardWrapperShimmer]}
      onClick={handleOnCardClick}
    >
      <div css={styles.avatar}>
        <Image src={transformPokeAvatar(id)} width={100} height={100} alt={'poke'} lazy />
      </div>
      <div css={styles.infoWrapper}>
        <div>
          <h3 css={styles.name}>{name}</h3>
        </div>
        <div css={styles.infoInner}>
          <PokeCardInfo
            dataSource={[
              { label: 'Height', value: weight ? `${height}dm` : '-dm' },
              { label: 'Weight', value: weight ? `${weight}hg` : '-hg' },
            ]}
          />
        </div>
        <div css={styles.infoInner}>
          <PokeCardInfo
            dataSource={[
              {
                label: 'HP',
                value: `${stats[0].base_stat}`,
              },
              {
                label: 'Attack',
                value: `${stats[1].base_stat}`,
              },
              {
                label: 'Defence',
                value: `${stats[2].base_stat}`,
              },
            ]}
          />
        </div>
      </div>
      {onActionClick && (
        <Button
          css={styles.action}
          loading={action}
          shape={'rounded'}
          color={'primary'}
          size={'small'}
          onClick={handleActionClick}
        >
          {actionTitle || 'catch'}
        </Button>
      )}
    </Card>
  );
};

export type PokeCardProps = {
  shimmer?: boolean;
  actionTitle?: string;
  onActionClick?: (id: number) => void;
  path?: string;
  aliasName?: string;
} & PokemonListQuery['pokemonV2List'][number];

export default memo(PokeCard);
