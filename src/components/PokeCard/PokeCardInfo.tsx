import React, { memo } from 'react';

import { Space } from 'antd-mobile';

import * as styles from '@/components/PokeCard/styles';

const PokeCardInfo = ({ dataSource }: PokeCardInfoProps) => {
  return (
    <Space justify="evenly" block>
      {dataSource.map(({ label, value }) => (
        <div key={label} css={styles.labelContainer}>
          <p css={styles.labelInfo}>{label}</p>
          <span css={styles.valueInfo}>{value}</span>
        </div>
      ))}
    </Space>
  );
};

export type PokeCardInfoProps = { dataSource: { label: string; value: string }[] };

export default memo(PokeCardInfo);
