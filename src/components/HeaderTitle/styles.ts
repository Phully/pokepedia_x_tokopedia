import { css } from '@emotion/react';

export const wrapper = css`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
  height: 40px;
`;

export const headerTitle = css`
  font-size: 18px;
  margin: 0;
  height: 38px;
`;

export const actionButton = css`
  padding: 0;
`;
