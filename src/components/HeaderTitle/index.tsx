import React, { memo } from 'react';

import { Button } from 'antd-mobile';

import * as styles from '@/components/HeaderTitle/styles';

const HeaderTitle = ({ children, title, onSeeAllClick }: HeaderTitleProps) => {
  return (
    <div>
      <div css={styles.wrapper}>
        <h2 css={styles.headerTitle}>{title}</h2>
        {onSeeAllClick && (
          <Button
            css={styles.actionButton}
            shape={'rounded'}
            color={'primary'}
            fill={'none'}
            onClick={onSeeAllClick}
          >
            See All
          </Button>
        )}
      </div>
      {children}
    </div>
  );
};

export type HeaderTitleProps = {
  title: string;
  children?: React.ReactNode;
  onSeeAllClick?: () => void;
};

export default memo(HeaderTitle);
