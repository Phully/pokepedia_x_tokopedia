import { css } from '@emotion/react';

export const space = css`
  --gap-horizontal: 8px;
  margin-left: 6px;
`;
