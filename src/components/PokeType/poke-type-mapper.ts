import normal from '@/components/PokeType/assets/normal.png';
import fighting from '@/components/PokeType/assets/fighting.png';
import flying from '@/components/PokeType/assets/flying.png';
import poison from '@/components/PokeType/assets/poison.png';
import ground from '@/components/PokeType/assets/ground.png';
import rock from '@/components/PokeType/assets/rock.png';
import bug from '@/components/PokeType/assets/bug.png';
import ghost from '@/components/PokeType/assets/ghost.png';
import steel from '@/components/PokeType/assets/steel.png';
import fire from '@/components/PokeType/assets/fire.png';
import water from '@/components/PokeType/assets/water.png';
import grass from '@/components/PokeType/assets/grass.png';
import electric from '@/components/PokeType/assets/electric.png';
import psychic from '@/components/PokeType/assets/psychic.png';
import ice from '@/components/PokeType/assets/ice.png';
import dragon from '@/components/PokeType/assets/dragon.png';
import dark from '@/components/PokeType/assets/dark.png';
import fairy from '@/components/PokeType/assets/fairy.png';
import unknown from '@/components/PokeType/assets/unknown.png';
import shadow from '@/components/PokeType/assets/shadow.png';

type POKE_TYPE_MAPPER_PROPS = {
  [key: string]: undefined | { name: string; url: string };
};

const POKE_TYPE_MAPPER: POKE_TYPE_MAPPER_PROPS = {
  normal: {
    name: 'normal',
    url: normal,
  },
  fighting: {
    name: 'fighting',
    url: fighting,
  },
  flying: {
    name: 'flying',
    url: flying,
  },
  poison: {
    name: 'poison',
    url: poison,
  },
  ground: {
    name: 'ground',
    url: ground,
  },
  rock: {
    name: 'rock',
    url: rock,
  },
  bug: {
    name: 'bug',
    url: bug,
  },
  ghost: {
    name: 'ghost',
    url: ghost,
  },
  steel: {
    name: 'steel',
    url: steel,
  },
  fire: {
    name: 'fire',
    url: fire,
  },
  water: {
    name: 'water',
    url: water,
  },
  grass: {
    name: 'grass',
    url: grass,
  },
  electric: {
    name: 'electric',
    url: electric,
  },
  psychic: {
    name: 'psychic',
    url: psychic,
  },
  ice: {
    name: 'ice',
    url: ice,
  },
  dragon: {
    name: 'dragon',
    url: dragon,
  },
  dark: {
    name: 'dark',
    url: dark,
  },
  fairy: {
    name: 'fairy',
    url: fairy,
  },
  unknown: {
    name: 'unknown',
    url: unknown,
  },
  shadow: {
    name: 'shadow',
    url: shadow,
  },
};

export default POKE_TYPE_MAPPER;
