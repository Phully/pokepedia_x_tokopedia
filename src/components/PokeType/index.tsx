import React, { memo } from 'react';

import { Space } from 'antd-mobile';

import POKE_TYPE_MAPPER from '@/components/PokeType/poke-type-mapper';

import unknown from '@/components/PokeType/assets/unknown.png';

import * as styles from '@/components/PokeType/styles';

const PokeType = ({ dataSource }: PokeTypeProps) => {
  return (
    <Space css={styles.space} direction={'horizontal'} block>
      {dataSource.map((data, index) => (
        <img
          key={`${data}-${index}`}
          src={POKE_TYPE_MAPPER?.[data]?.url || unknown}
          alt={'type'}
          width={18}
          height={18}
        />
      ))}
    </Space>
  );
};

export type PokeTypeProps = {
  dataSource: string[];
};

export default memo(PokeType);
