import { css } from '@emotion/react';

export const virtualGridContainer = css`
  position: absolute;
  width: 100%;
  height: 100%;
  overflow: auto;
`;

export const virtualGridWrapper = css`
  position: relative;
  margin: 0 16px;
  > div {
    position: relative;
    padding-bottom: 10px;
  }
  > div:last-child {
    padding-bottom: 35px;
  }
`;
