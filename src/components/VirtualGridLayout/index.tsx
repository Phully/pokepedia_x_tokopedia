import React, { memo, useRef, useImperativeHandle, forwardRef } from 'react';

import { Grid } from 'antd-mobile';
import { useCreation, useVirtualList } from 'ahooks';

import PokeCard from '@/components/PokeCard';
import ListenerLoadMore from '@/components/ListenerLoadMore';

import transformPokeGrid from '@/utils/functions/poke-grid';

import type { PokemonListQuery } from '@/codegen/graphql';
import { virtualGridContainer, virtualGridWrapper } from '@/components/VirtualGridLayout/styles';

const VirtualGridLayout = forwardRef<VirtualGridLayoutRef, VirtualGridLayoutProps>(
  ({ actionTitle, dataSource, offset = 10, onActionClick, onLoadMore }, ref) => {
    const wrapperRef = useRef(null);
    const containerRef = useRef(null);

    const pokemonListMemo = useCreation(() => {
      return transformPokeGrid<PokemonListQuery['pokemonV2List'][number]>(dataSource);
    }, [dataSource]);

    const [list, scrollTo] = useVirtualList(pokemonListMemo, {
      containerTarget: containerRef,
      wrapperTarget: wrapperRef,
      itemHeight: 285,
      overscan: 10,
    });

    useImperativeHandle(ref, () => ({
      scrollVirtualGrid: (index: number) => {
        scrollTo(index);
      },
    }));

    return (
      <div ref={containerRef} css={virtualGridContainer}>
        <div ref={wrapperRef} css={virtualGridWrapper}>
          {list.map(({ data, index }) => (
            <Grid key={`pokemon-list-${index}`} columns={2} gap={20}>
              {data.map((pokemon) => (
                <Grid.Item key={`pokemon-grid-${pokemon.id}`}>
                  <PokeCard {...pokemon} actionTitle={actionTitle} onActionClick={onActionClick} />
                </Grid.Item>
              ))}
              {pokemonListMemo.length % offset === 0 && index === pokemonListMemo.length - 1 && (
                <ListenerLoadMore key={`loader-pokemon-${index}`} onLoadMore={onLoadMore} />
              )}
            </Grid>
          ))}
        </div>
      </div>
    );
  },
);

export type VirtualGridLayoutProps = {
  actionTitle?: string;
  dataSource: PokemonListQuery['pokemonV2List'][number][];
  offset?: number;
  onLoadMore?: () => Promise<void>;
  onActionClick?: (id: number) => void;
};

export type VirtualGridLayoutRef = {
  scrollVirtualGrid: (index: number) => void;
};

export default memo(VirtualGridLayout);
