import styled from '@emotion/styled';

import { css } from '@emotion/react';

export const loadMoreWrapper = css`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
`;

export const loadMoreLoading = css`
  display: flex;
  justify-content: center;
  font-size: 24px;
  margin: 16px 20px 0;
`;

export const LoadMoreDiv = styled.div((props: { show: boolean }) => ({
  display: props.show ? 'unset' : 'none',
}));
