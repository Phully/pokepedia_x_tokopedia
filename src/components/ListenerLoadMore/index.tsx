import React, { useRef, useState, memo } from 'react';

import { DotLoading } from 'antd-mobile';
import { useInViewport, useAsyncEffect } from 'ahooks';

import { LoadMoreDiv, loadMoreLoading, loadMoreWrapper } from './styles';

const ListenerLoadMore = ({ onLoadMore }: ListenerLoadMoreProps) => {
  const ref = useRef(null);
  const [show, setShow] = useState(true);
  const [inViewport] = useInViewport(ref);

  useAsyncEffect(async () => {
    if (inViewport && onLoadMore) {
      await setShow(false);
      await onLoadMore();
      await setShow(true);
    }
  }, [inViewport, onLoadMore]);

  return (
    <>
      {onLoadMore && (
        <div css={loadMoreWrapper}>
          <LoadMoreDiv ref={ref} show={show} />
          {show ? <DotLoading css={loadMoreLoading} color="primary" /> : null}
        </div>
      )}
    </>
  );
};

export type ListenerLoadMoreProps = {
  onLoadMore?: () => Promise<void>;
};

export default memo(ListenerLoadMore);
