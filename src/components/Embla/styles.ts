import { css } from '@emotion/react';

export const embla = css`
  position: relative;
  max-width: 500px;
  margin-right: auto;
  margin-left: auto;
`;

export const emblaViewport = css`
  width: 100%;
  overflow: hidden;
  .is-draggable {
    cursor: grab;
  }
  .is-dragging {
    cursor: grabbing;
  }
`;

export const emblaContainer = css`
  display: flex;
  margin-left: -16px;
  user-select: none;
  -webkit-touch-callout: none;
  -webkit-tap-highlight-color: transparent;
`;
