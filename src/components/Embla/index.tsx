import React, { memo } from 'react';

import useEmblaCarousel from 'embla-carousel-react';

import type { EmblaOptionsType } from 'embla-carousel-react';

import * as styles from '@/components/Embla/styles';

const Embla = ({
  children = null,
  options = { dragFree: true, containScroll: 'trimSnaps' },
}: EmblaProps) => {
  const [viewportRef] = useEmblaCarousel(options);

  return (
    <div css={styles.embla}>
      <div ref={viewportRef} css={styles.emblaViewport}>
        <div css={styles.emblaContainer}>{children}</div>
      </div>
    </div>
  );
};

export type EmblaProps = {
  children: React.ReactNode;
  options?: EmblaOptionsType;
};

export default memo(Embla);
